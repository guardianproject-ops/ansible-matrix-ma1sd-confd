# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.2.0](https://gitlab.com/guardianproject-ops/ansible-matrix-ma1sd-confd/compare/v0.1.0...v0.2.0) (2020-05-14)


### ⚠ BREAKING CHANGES

* Switch to ma1sd

### Features

* Switch to ma1sd ([69c4ada](https://gitlab.com/guardianproject-ops/ansible-matrix-ma1sd-confd/commit/69c4adaa14170affd754ff7487c1b7bb7da07a66))

## 0.1.0 (2019-05-27)

* last version as mxisd
