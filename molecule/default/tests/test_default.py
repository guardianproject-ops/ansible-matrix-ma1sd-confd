import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_directories(host):
    dirs = [
            "/etc/ma1sd",
            "/etc/confd",
            "/etc/confd/conf.d",
            "/etc/confd/templates"
    ]
    for dir in dirs:
        d = host.file(dir)
        assert d.is_directory
        assert d.exists


def test_files(host):
    files = [
            "/etc/systemd/system/ma1sd.service",
            "/etc/confd/templates/ma1sd.yaml.tmpl",
            "/etc/confd/conf.d/ma1sd.yaml.toml"
    ]
    for file in files:
        f = host.file(file)
        assert f.exists
        assert f.is_file


def test_service(host):
    s = host.service("ma1sd")
    assert not s.is_running
    # This is not working..
    # assert s.is_enabled


def test_socket(host):
    sockets = [
    ]
    for socket in sockets:
        s = host.socket(socket)
        assert s.is_listening
